Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "bug" label.

Verify the issue you're about to submit isn't a duplicate.

Please remove this notice if you're confident your issue isn't a duplicate.

------

### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)

### What environment? (eg: Staging, Production)

(What url/environment did you experience this bug)
Note: Till we do not have any domain names, please specify `Staging` or `Production`.

### What is the current *bug* behavior?

(What actually happens)

### What is the expected *correct* behavior?

(What you should see instead)

### Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

### Screenshots

(Please upload relevant screenshots of the error you are experiencing)

/label ~bug
