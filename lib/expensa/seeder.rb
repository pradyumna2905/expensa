module Expensa
  class Seeder
    def self.stdout(message, color = :green)
      puts message.color(color)
    end

    def self.quiet!
      SeedFu.quiet = true
    end

    def self.print_result(resource)
      return if resource.blank?

      if resource.persisted?
        print '.'.color(:green)
      else
        print 'F'.color(:red)
      end
    end
  end
end
