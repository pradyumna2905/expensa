require 'rails_helper'

RSpec.describe ImportsController, type: :controller do
  describe '#new' do
    context 'when signed out' do
      it 'does not render new template' do
        get :new

        expect(response).to redirect_to new_user_session_path
      end
    end

    context 'when signed in' do
      it 'renders new template' do
        user = create(:user)
        sign_in user

        get :new

        expect(response).to render_template :new
      end
    end
  end

  describe '#create' do
    include FileMocker

    let(:user) { create(:user) }

    before do
      sign_in user
    end

    context 'with valid params' do
      let(:file) { Rack::Test::UploadedFile.new(File.open(
        "#{Rails.root}/spec/factories/valid_import_file.csv"
      )) }

      it 'creates import record' do

        expect { post :create, params:
                 {file: file, payment_method: user.payment_methods.last.id}}.
                 to change(Import, :count).by 1

      end

      it 'redirects to index page' do
        post :create, params: {file: file, payment_method: user.payment_methods.last.id}

        expect(response).to redirect_to transactions_path
      end
    end

    context 'with invalid params' do
      let(:file) { Rack::Test::UploadedFile.new(File.open(
        "#{Rails.root}/spec/factories/invalid_import_file.csv"
      )) }

      it 'does not creates transaction record' do
        expect { post :create, params:
                 {file: file, payment_method: user.payment_methods.last.id}}.
                 to_not change(Import, :count)

      end

      it 'renders the new template' do
        post :create, params: {file: file, payment_method: user.payment_methods.last.id}

        expect(response).to render_template :new
      end
    end
  end
end
