require 'rails_helper'

RSpec.describe TransactionsController, type: :controller do
  describe '#new' do
    context 'when signed in' do
      it 'renders new page' do
        user = create(:user)
        sign_in user
        get :new

        expect(response).to render_template :new
      end
    end

    context 'when signed out' do
      it 'redirects to log in page' do
        get :new

        expect(response).to redirect_to(new_user_session_path)
      end

      it 'populates flash with message' do
        get :new

        expect(flash[:alert]).
          to eq "You need to sign in or sign up before continuing."
      end
    end
  end

  describe '#create' do
    let(:user) { create_and_sign_in_user }

    context 'with valid params' do
      it 'creates transaction record' do
        expect { post :create, params:
                 { transaction: attributes_for(:transaction, user: user) } }.
                 to change(Transaction, :count).by 1

      end

      it 'redirects to index page' do
        post :create, params: { transaction: attributes_for(:transaction,
                                                        user: user) }

        expect(response).to redirect_to transactions_path
      end
    end

    context 'with invalid params' do
      it 'does not creates transaction record' do
        expect { post :create, params:
                 { transaction: attributes_for(:transaction,
                                           user: user,
                                           date: Date.tomorrow) } }.
                 to_not change(Transaction, :count)

      end

      it 'renders the new template' do
        post :create, params: { transaction: attributes_for(:transaction,
                                                        amount: nil,
                                                        user: user) }

        expect(response).to render_template :new
      end
    end
  end

  describe '#index' do
    context 'when signed in' do
      let(:user) { create_and_sign_in_user }
      let!(:transaction_current) { create(:transaction, user: user,
                                          description: "today",
                                          date: Date.current) }
      let!(:transaction_today) { create(:transaction, user: user,
                                        description: "today",
                                        date: Date.new(2017, 5, 15)) }
      let!(:transaction_oldest) {  create(:transaction, user: user,
                                          description: "oldest",
                                          date: Date.new(2017, 5, 1)) }
      let!(:transaction_old) {  create(:transaction, user: user,
                                       description: "old",
                                       date: Date.new(2017, 5, 14)) }
      let!(:income_transaction) { create(:transaction,
                                         :credit,
                                         description: "income",
                                         date: Date.new(2017, 5, 13),
                                         user: user) }

      before do
        # Adding params here so we can have consistent results. If we use
        # Date.current and 4.days ago to create transactions, tests will be
        # flaky as they will fail when it's the 1st of any given month.
        get :index, params: {date: {month: 5, year: 2017} }
      end

      it 'renders index page' do
        expect(response).to render_template :index
      end

      it 'displays all the transactions in desc order' do
        expect(assigns(:transactions).map(&:description)).to eq(
          ["today", "old", "income", "oldest"]
        )
      end

      it 'calculates the total expense for the month' do
        expect(assigns(:monthly_expenses)).to eq 134.97
      end

      it 'calculates the total income for the month' do
        expect(assigns(:monthly_income)).to eq 44.99
      end

      it 'calculates total expense of current month if no params in url' do
        create(:transaction, user: user)

        get :index

        expect(assigns(:monthly_expenses)).to eq 89.98
      end

      it 'calculates total income of current month if no params in url' do
        create(:transaction, :credit, user: user)

        get :index

        expect(assigns(:monthly_income)).to eq 44.99
      end
    end

    context 'when signed out' do
      it 'renders sign in page' do
        get :index

        expect(response).to redirect_to new_user_session_path
      end

      it 'populates flash with message' do
        get :index

        expect(flash[:alert]).
          to eq "You need to sign in or sign up before continuing."
      end
    end
  end

  describe '#edit' do
    context 'when signed in' do
      it 'renders edit page' do
        user = create_and_sign_in_user
        transaction = create(:transaction, user: user)
        get :edit, params: { id: transaction.id }

        expect(response).to render_template :edit
        expect(assigns(:transaction)).to eq transaction
      end
    end

    context 'when signed out' do
      it 'renders sign in page' do
        get :edit, params: { id: 5 }

        expect(response).to redirect_to new_user_session_path
      end

      it 'populates flash with message' do
        get :edit, params: { id: 5 }

        expect(flash[:alert]).
          to eq "You need to sign in or sign up before continuing."
      end
    end
  end

  describe '#update' do
    context 'with valid params' do
      it 'creates a transaction record' do
        user = create_and_sign_in_user
        transaction = create(:transaction, user: user)

        put :update, params: { id: transaction.id,
                               transaction: attributes_for(:transaction,
                                                       user: user,
                                                       description: "New") }

        expect(transaction.reload.description).to eq "New"
        expect(response).to redirect_to transactions_path
      end
    end

    context 'with invalid params' do
      it 'does not creates transaction record' do
        user = create_and_sign_in_user
        transaction = create(:transaction, description: "Old", user: user)

        put :update, params: { id: transaction.id,
                               transaction: attributes_for(:transaction,
                                                       user: user,
                                                       description: "") }

        expect(transaction.reload.description).to eq "Old"
        expect(response).to render_template :edit
      end
    end
  end

  describe '#destroy' do
    it 'finds the right record' do
      user = create_and_sign_in_user
      transaction = create(:transaction, description: "Old", user: user)

      delete :destroy, params: { id: transaction.id }

      expect(assigns(:transaction)).to eq transaction
    end

    it 'deletes the right record' do
      user = create_and_sign_in_user
      transaction = create(:transaction, description: "Old", user: user)

      expect { delete :destroy,
               params: { id: transaction.id } }.
      to change(Transaction, :count).by -1
    end

    it 'redirects to transactions index' do
      user = create_and_sign_in_user
      transaction = create(:transaction, description: "Old", user: user)

      delete :destroy, params: { id: transaction.id }

      expect(response).to redirect_to transactions_path
    end
  end

  def create_and_sign_in_user
    user = create(:user)
    sign_in user
    user
  end
end
