require 'rails_helper'

RSpec.describe DashboardsController, type: :controller do
  describe '#current' do
    context 'when signed in' do
      before do
        create_and_sign_in_user
      end

      it 'renders the current page' do
        get :current

        expect(response).to render_template :current
      end

      it 'returns transactions based on valid selected month and year' do
        expense_may1 = create(:transaction, description: "May1", user: @user, date: Date.new(2017, 5, 15))
        expense_may2 = create(:transaction, description: "May2", user: @user, date: Date.new(2017, 5, 25))
        expense_june1 = create(:transaction, user: @user, date: Date.new(2017, 6, 25))

        get :current, params: {date: {year: 2017, month: 5} }

        expect(assigns(:transactions).map(&:description)).to eq(
          ["May2", "May1"]
        )
      end

      it 'returns current month transactions on invalid selected month and year' do
        expense_current1 = create(:transaction, description: "Current", user: @user)
        expense_may2 = create(:transaction, description: "May2", user: @user, date: Date.new(2017, 5, 25))
        expense_june1 = create(:transaction, user: @user, date: Date.new(2017, 6, 25))

        get :current, params: {date: {year: 20188, month: 51} }

        expect(assigns(:transactions).map(&:description)).to eq(
          ["Current"]
        )
      end

      it 'returns current month transactions when no params present' do
        expense_current1 = create(:transaction, description: "Current", user: @user)
        expense_may2 = create(:transaction, description: "May2", user: @user, date: Date.new(2017, 5, 25))
        expense_june1 = create(:transaction, user: @user, date: Date.new(2017, 6, 25))

        get :current

        expect(assigns(:transactions).map(&:description)).to eq(
          ["Current"]
        )
      end
    end

    context 'when signed out' do
      it 'does not render current tempalte' do
        get :current

        expect(response).to_not render_template :current
      end

      it 'redirects to log in page' do
        get :current

        expect(response).to redirect_to(new_user_session_path)
      end

      it 'populates flash with message' do
        get :current

        expect(flash[:alert]).
          to eq "You need to sign in or sign up before continuing."
      end
    end
  end

  describe '#trends' do
    context 'when signed in' do
      before do
        create_and_sign_in_user
      end

      it 'renders the trends page' do
        get :trends

        expect(response).to render_template :trends
      end
    end

    context 'when signed out' do
      it 'does not render trends tempalte' do
        get :trends

        expect(response).to_not render_template :current
      end

      it 'redirects to log in page' do
        get :trends

        expect(response).to redirect_to(new_user_session_path)
      end

      it 'populates flash with message' do
        get :trends

        expect(flash[:alert]).
          to eq "You need to sign in or sign up before continuing."
      end
    end
  end

  def create_and_sign_in_user
    @user = create(:user)
    sign_in @user
  end
end

