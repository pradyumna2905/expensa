require 'rails_helper'

describe 'User filters transactions by category' do
  let(:user) { create(:user) }
  let!(:category) { create(:category, title: 'Groceries') }
  let!(:grocery_transaction_may1) { create(:transaction,
                                           date: Date.new(2017, 5, -1),
                                           category: category,
                                           user: user) }
  let!(:grocery_transaction_may2) { create(:transaction,
                                           date: Date.new(2017, 5, -1),
                                           category: category,
                                           user: user) }
  let!(:transactions_may) do
    2.times do
      create(:transaction,
              date: Date.new(2017, 5, -1),
              category: create(:category, title: 'Certainly not Groceries'),
              user: user)
    end
  end
  let!(:housing_transaction_current) { create(:transaction,
                                      date: Date.current,
                                      category: create(
                                        :category,
                                        title: 'Housing'
                                      ),
                                      user: user) }
  let!(:transaction_current) { create(:transaction,
                                      date: Date.current,
                                      user: user) }

  before do
    sign_in user
    visit transactions_path
  end

  context 'with a date selected' do
    it 'returns the transactions within the date range and matching category' do
      expect(page).to have_content(Date.current.month)
      expect(page).to have_content(Date.current.year)

      select_this_from_that('May', 'date_month')
      select_this_from_that('2017', 'date_year')
      click_on 'View'

      expect(page).to have_content('May 31, 2017')
      expect(page).to have_content('Groceries')
      expect(page).to have_content('Certainly not Groceries')

      # filter by 'Groceries'
      first(:link, 'Groceries').click

      expect(page).to_not have_content('Certainly not Groceries')
    end
  end

  context 'without a date selected' do
    it 'returns transactions within current datetime matching category' do
      expect(page).to have_content('Housing')
      expect(page).to have_content('Uncategorized')

      # Filter by 'Housing'
      click_on 'Housing'

      expect(page).to_not have_content('Uncategorized')
    end
  end
end
