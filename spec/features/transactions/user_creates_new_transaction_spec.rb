require 'rails_helper'

describe 'User creates new transaction' do
  let(:user) { create(:user) }

  before do
    sign_in user
    visit new_transaction_path
  end

  context 'expense with valid data' do
    it 'saves the transaction successfully' do
      create_transaction({
        kind: 'debit',
        date_of_month: '29',
        month: 'May',
        year: '2017',
        amount: '10'
      })

      select_this_from_that("2017", "date_year")
      select_this_from_that("May", "date_month")
      submit_form

      expect(page).to have_content("Beer")
      expect(page).to have_content("May 29, 2017")
      expect(page).to have_content("Cash")
      expect(page).to have_content("Uncategorized")
      expect(page).to have_content("-$10.00")
    end
  end

  context 'income with valid data' do
    it 'saves the transaction successfully' do
      create_transaction({
        kind: 'credit',
        date_of_month: '29',
        month: 'May',
        year: '2017',
        amount: '10'
      })

      select_this_from_that("2017", "date_year")
      select_this_from_that("May", "date_month")
      submit_form

      expect(page).to have_content("Beer")
      expect(page).to have_content("May 29, 2017")
      expect(page).to have_content("Cash")
      expect(page).to have_content("Uncategorized")
      expect(page).to have_content("+$10.00")
    end
  end

  context 'with invalid data' do
    it 'does not save the transaction successfully' do
      expect(page).to have_content("New Transaction")

      select_this_from_that("2020", "transaction_date_1i")
      select_this_from_that("May", "transaction_date_2i")
      select_this_from_that("29", "transaction_date_3i")
      fill_in_this_with_that("Description", "")
      fill_in_this_with_that("Amount", nil)
      expect(page).to have_select("transaction_category_id", options: ["Uncategorized"])
      expect(page).to have_select("transaction_payment_method_id", options: ["Cash"])

      submit_form

      expect(page).to have_content("New Transaction")
      expect(page).to have_content("can\'t be in the future.")
      expect(page).to have_content("can\'t be blank")
    end
  end

  context 'and chooses to create another' do
    it 'creates the first transaction' do
      create_transaction(
        {
          kind: 'debit',
          date_of_month: '29',
          month: 'May',
          year: '2017',
          amount: '10',
          description: 'First Transaction'
        },
        create_another: true
      )

      expect(page).to have_content('New Transaction')
      expect(Transaction.last.description).to eq 'First Transaction'

      create_transaction({
        description: 'Second Transaction'
      })

      expect(page).to have_content 'Second Transaction'
    end
  end
end

# Creates debit or credit transaction
# Default date selection is current date.
# You can pass in date, month, year to this method like such.
#
# create_transaction({date: "29", month: "May", year: "2017")
#
# Any field left out will result it being set to current value.
def create_transaction(attributes, create_another: false)
  attributes = attributes.reverse_merge(default_transaction_attributes)

  # Delete the date from the hash because it's a datetime object and we want to
  # select it using dropdown.
  attributes.delete(:date)
  year = attributes[:year] || Date.current.year.to_s
  month = attributes[:month] || Date.current.strftime("%B")
  date = attributes[:date_of_month] || Date.current.strftime("%d")

  expect(page).to have_content("New Transaction")

  select(attributes[:kind])
  select_this_from_that(year, "transaction_date_1i")
  select_this_from_that(month, "transaction_date_2i")
  select_this_from_that(date, "transaction_date_3i")
  fill_in_this_with_that('Description', attributes[:description])
  fill_in_this_with_that('Amount', attributes[:amount])

  expect(page).to have_select("transaction_category_id", options: ["Uncategorized"])
  expect(page).to have_select("transaction_payment_method_id", options: ["Cash"])

  find(:css, '#transaction_create_another').set(create_another)

  submit_form
end

def default_transaction_attributes
  attributes_for(:transaction, user: create(:user))
end
