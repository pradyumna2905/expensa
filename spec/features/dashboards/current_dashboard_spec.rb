require 'rails_helper'

describe 'User visits their trends' do
  let(:user) { create(:user) }

  before do
    sign_in user
    visit dashboards_current_path

    expect(current_path).to eq dashboards_current_path

    click_link "TRENDS"
    expect(current_path).to eq dashboards_trends_path

    expect(page).to have_content "#{Date.current.strftime('%B %Y')}"
  end

  # TODO
  context 'when user has no transactions for the past week' do
    xit 'displays no-transactions message' do
      expect(current_path).to eq dashboards_current_path

      click_link "TRENDS"
      expect(page).to have_content "#{Date.current.strftime('%B %Y')} transactions"
      expect(page).
        to have_content "You do not have any transactions in the past week. Yay!"
    end
  end

  context 'when the user has transactions', :js do
    it 'displays the line chart and pie chart' do
      it_displays_the_graphs
    end

  context 'user selects month and year', :js do
    it 'displays the line chart and pie chart' do
      select_this_from_that("May", "date_month")
      select_this_from_that("2017", "date_year")
      submit_form

      expect(page).to have_content("May 2017")

      it_displays_the_graphs
    end
  end
  end

  def it_displays_the_graphs
    # Chartkick assigns sequential ids to charts. So this is the line chart.
    expect(page).to have_css('div#chart-1')
    # And the pie charts
    expect(page).to have_css('div#chart-2')
    expect(page).to have_css('div#chart-3')
  end
end

