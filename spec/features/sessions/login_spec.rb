require 'rails_helper'

describe 'User logs in' do
  let(:user) { create(:user) }

  before do
    visit root_path
    click_link "Log in"
  end

  context 'with valid credentials' do
    it 'logs in user successfully' do
      fill_in_this_with_that("Email", user.email)
      fill_in_this_with_that("Password", user.password)

      submit_form

      expect(page).to have_content("CURRENT")
      expect(page).to have_content("TRENDS")
    end
  end

  context 'with invalid credentials' do
    before do
      fill_in_this_with_that("Email", "john@example.com")
      fill_in_this_with_that("Password", "wrong_password")

      submit_form
    end

    it 'does not log in user' do
      expect(page).to have_content("Log in")
    end

    it 'shows and error' do
      expect(page).to have_content("Invalid Email or password.")
    end
  end
end

describe 'User is redirected to correct page after sign in' do
  let(:user) { create(:user) }

  context 'when user visits non-default after-sign-in page' do
    before do
      visit new_transaction_path
    end

    it 'redirects user to log in page' do
      expect(page).to have_content("Log in")
    end

    it 'displays login required message' do
      expect(page).
        to have_content("You need to sign in or sign up before continuing.")
    end

    it 'redirects to the refered page after loggin in' do
      expect(page).to have_content("Log in")

      fill_in_this_with_that("Email", user.email)
      fill_in_this_with_that("Password", user.password)

      submit_form

      expect(page).to have_current_path(new_transaction_path)
    end
  end
end
