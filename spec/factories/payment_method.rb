FactoryGirl.define do
  factory :payment_method do
    user
    name { 'Chase' }
  end
end
