FactoryGirl.define do
  factory :import do
    filename "test.csv"
    content_type "text/csv"
    user
  end
end
