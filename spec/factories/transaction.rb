FactoryGirl.define do
  factory :transaction do
    user
    date { Date.current }
    description { 'Beer' }
    amount { 44.99 }
    payment_method_id { user.payment_methods.first.id }
    category_id { user.categories.first.id }
    kind 'debit'

    trait :credit do
      kind "credit"
    end
  end
end
