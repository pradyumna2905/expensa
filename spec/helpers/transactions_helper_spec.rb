require 'spec_helper'

describe TransactionsHelper do
  describe '#transaction_deletion_message' do
    let(:transaction) { create(:transaction,
                               user: create(:user)) }

    it do
      expect(transaction_deletion_message(transaction)).to eq(
        "Are you sure you want to delete this transaction of $44.99?"
      )
    end
  end
end
