require 'spec_helper'

describe ProfilesHelper do
  it { expect(fake_masked_password).to eq '********' }

  describe '#category_deletion_message' do
    let(:category) { create(:category, user: create(:user)) }

    it do
      expect(category_deletion_message(category)).to eq(
        'Are you sure you want to delete Utility category?'
      )
    end
  end

  describe '#payment_method_deletion_message' do
    let(:payment_method) { create(:payment_method, user: create(:user)) }

    it do
      expect(payment_method_deletion_message(payment_method)).to eq(
        'Are you sure you want to delete Chase as your payment method?'
      )
    end
  end
end
