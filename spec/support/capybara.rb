require 'capybara/rails'
require 'capybara/rspec'
require 'selenium-webdriver'

Capybara.register_driver :chrome do |app|
  options = Selenium::WebDriver::Chrome::Options.new
  options.add_argument("window-size=1240,1400")

  options.add_argument("no-sandbox")

  # Run headless by default unless CHROME_HEADLESS specified
  unless ENV['CHROME_HEADLESS'] =~ /^(false|no|0)$/i
    options.add_argument("headless")

    # Chrome documentation says this flag is needed for now
    # https://developers.google.com/web/updates/2017/04/headless-chrome#cli
    options.add_argument("disable-gpu")
  end

  Capybara::Selenium::Driver.new(
    app,
    browser: :chrome,
    options: options
  )
end

Capybara.javascript_driver = :chrome

RSpec.configure do |config|
  config.before(:example, :js) do
    session = Capybara.current_session

    allow(ExpenseTracker::Application.routes).to receive(:default_url_options).and_return(
      host: session.server.host,
      port: session.server.port,
      protocol: 'http'
    )
  end
end
