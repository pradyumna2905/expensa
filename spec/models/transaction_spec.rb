require 'rails_helper'

RSpec.describe Transaction, type: :model do
  it { should belong_to :user }
  it { should belong_to :payment_method }
  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:amount) }
  it { should define_enum_for(:kind).with([:debit, :credit]) }

  it { should validate_numericality_of(:amount).is_greater_than(0) }
  it { should validate_length_of(:description).is_at_least(2) }

  describe ".highest_expense_transactions" do
    let(:user) { create(:user) }
    let!(:higest_amount_expense) { create(:transaction,
                                          description: 'highest',
                                          user: user,
                                          amount: 100) }
    let!(:low_amount_expense) { create(:transaction,
                                       description: 'low',
                                       user: user,
                                       amount: 10) }
    let!(:high_amount_expense) { create(:transaction,
                                        description: 'high',
                                        user: user,
                                        amount: 50) }
    let!(:highest_amount_income) { create(:transaction,
                                          :credit,
                                          description: 'highest_income',
                                          user: user,
                                          amount: 1000) }

    it "returns highest amount expenses in desc order" do
      expect(user.transactions.highest_expense_transactions.map(&:description)).to eq(
        ["highest", "high", "low"]
      )
    end

    context "when limit is specified in the argument" do
      it "returns number of records specified in the limit argument" do
        # When limit is 2
        expect(user.transactions.highest_expense_transactions(2).map(&:description)).to eq(
          ["highest", "high"]
        )

        # When limit is 500 (more than the records present)
        expect(user.transactions.highest_expense_transactions(500).map(&:description)).to eq(
          ["highest", "high", "low"]
        )
      end
    end
  end

  describe ".not_in_future" do
    it "validates that date entered is not in future" do
      user = create(:user)
      transaction = build(:transaction,
                      user: create(:user),
                      date: 2.days.from_now)

      expect(transaction).to_not be_valid
    end
  end

  describe '.filter_by_params' do
    let(:user) { create(:user) }
    let(:category) { create(:category, title: 'Groceries') }
    let(:transaction_may) { create(:transaction,
                                   date: Date.new(2017, 5, -1),
                                   user: user) }
    let(:transaction_jan) { create(:transaction,
                                   date: Date.new(2018, 1, -1),
                                   category: category,
                                   user: user) }
    let(:transaction_current) { create(:transaction,
                                       date: Date.current,
                                       category: category,
                                       user: user) }

    context 'when only the date param is provided' do
      let(:index_params) { {month: 5, year: 2017} }

      it 'returns transactions within the provided date range' do
        expect(
          user.transactions.filter_by_params(index_params)
        ).to match_array [transaction_may]
      end
    end

    context 'when only the category param is provided' do
      let(:index_params) { {category: 'Groceries'} }

      it 'returns all transactions matching the category' do
        expect(
          user.transactions.filter_by_params(index_params)
        ).to match_array [transaction_current]
      end
    end

    context 'when date and category are provided' do
      let(:index_params) { {month: 1, year: 2018, category: 'Groceries'} }

      it 'returns transactions within date range and matching the category' do
        expect(
          user.transactions.filter_by_params(index_params)
        ).to match_array [transaction_jan]
      end
    end
  end

  describe '.by_month' do
    context 'when passed valid params' do
      it 'returns the transactions for that month and year' do
        user = create(:user)
        month_transaction = create(:transaction, user: user, date: 60.days.ago)
        excluded_month_transaction = create(:transaction,
                                        user: user,
                                        date: Date.current)

        # Passed in as strings
        expect(user.transactions.by_month(2.months.ago.strftime("%B"),
                                          2.months.ago.year)).
                                          to_not include excluded_month_transaction
        expect(user.transactions.by_month(2.months.ago.strftime("%B"),
                                          2.months.ago.year)).
                                          to include month_transaction
      end
    end

    context 'when passed invalid params' do
      it 'returns current months transactions' do
        user = create(:user)
        current_month_transaction = create(:transaction, user: user, date: Date.current)
        prev_month_transaction = create(:transaction,
                                    user: user,
                                    date: 1.month.ago)

        expect(user.transactions.by_month("Mayy", nil)).
          to include current_month_transaction
        expect(user.transactions.by_month(nil, nil)).
          to_not include prev_month_transaction
      end
    end
  end

  describe '.total_expense' do
    it 'returns the total expenses for the transactions' do
      user = create(:user)
      create(:transaction, user: user, amount: 15, date: Date.current)
      create(:transaction, user: user, amount: 30, date: Date.current)
      create(:transaction, amount: 100, user: user, date: 1.month.ago)
      create(:transaction, :credit, amount: 500,
              user: user, date: 2.days.ago)

      expect(user.transactions.total_expense).to eq 145
    end
  end

  describe '.total_income' do
    it 'returns the total income for the transactions' do
      user = create(:user)
      create(:transaction, user: user, amount: 15, date: Date.current)
      create(:transaction, :credit, user: user, amount: 30, date: Date.current)
      create(:transaction, amount: 100, user: user, date: 1.month.ago)
      create(:transaction, :credit, amount: 500,
              user: user, date: 2.days.ago)

      expect(user.transactions.total_income).to eq 530
    end
  end
end
