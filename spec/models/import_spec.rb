require 'rails_helper'

RSpec.describe Import, type: :model do
  it { should have_many :transactions }
  it { should validate_uniqueness_of :filename }
  it { should accept_nested_attributes_for :transactions }
  it { should belong_to :user }
end
