require 'rails_helper'

describe Imports::CSVParseService do
  include FileMocker

  describe "#execute" do
    let(:user) { create(:user) }
    let(:import) { create(:import, user: user) }

    after(:each) do
      clean_up_files
    end

    before do
      @response = service.execute
    end

    context 'when headers do not match expected headers' do
      let(:service) { described_class.new(import, {file: invalid_csv_test_file}) }

      it 'sets response status to fail' do
        expect(@response[:status]).to eq :fail
      end
    end

    context 'when headers match expected headers' do
      let(:service) { described_class.new(import, {file: valid_csv_test_file}) }

      it 'returns data' do
        expect(@response[:data]).to eq [{"date"=>"11/11/2017", "amount"=>-89.0, "description"=>"description"}]
      end

      it 'sets status to ok' do
        expect(@response[:status]).to eq :ok
      end
    end
  end
end

