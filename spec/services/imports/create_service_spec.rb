require 'rails_helper'

describe Imports::CreateService do
  describe "#execute" do
    include FileMocker

    let(:user) { create(:user) }

    context "when CSV is valid" do
      let(:service) { described_class.new(user, {file: valid_csv_test_file, payment_method: user.payment_methods.last}) }

      before do
        @import = service.execute
      end

      it 'creates import record' do
        expect(Import.count).to eq 1
      end
    end

    context "when CSV is invalid" do
      let(:service) { described_class.new(user, {file: invalid_csv_test_file, payment_method: user.payment_methods.last}) }

      it 'creates import record' do
        expect { service.execute }.
                 to change(Import, :count).by 0
      end
    end
  end
end
