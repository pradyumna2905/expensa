module PaymentMethods
  class UpdateService < PaymentMethods::BaseService
    def initialize(payment_method, params = {})
      @payment_method = payment_method
      @params = params.dup
    end

    def execute
      @payment_method.tap do |payment_method|
        payment_method.update!(@params)
      end

    rescue ActiveRecord::RecordInvalid => e
      fail(error: e.message, resource: @payment_method)
    rescue StandardError => e
      fail(error: e.message, resource: @payment_method)
    end
  end
end
