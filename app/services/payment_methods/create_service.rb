module PaymentMethods
  class CreateService < PaymentMethods::BaseService
    def execute
      @payment_method = @current_user.payment_methods.build(@params)

      @payment_method.tap do |payment_method|
        payment_method.save!
      end

    rescue ActiveRecord::RecordInvalid => e
      fail(error: e.message, resource: @payment_method)
    rescue StandardError => e
      fail(error: e.message, resource: @payment_method)
    end
end
