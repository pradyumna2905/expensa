module PaymentMethods
  class DestroyService < PaymentMethods::BaseService
    def initialize(user, payment_method)
      @current_user = user
      @payment_method = payment_method
    end

    def execute
      @payment_method.tap do |payment_method|
        ActiveRecord::Base.transaction do
          # First roll back transactions with this payment method to default
          # payment method
          sync_transactions!

          payment_method.destroy!
        end
      end

    rescue StandardError => e
      fail(error: e.message, resource: @payment_method)
    end

    private

    def sync_transactions!
      Transaction.rollback_to_default_resource(@current_user, @payment_method)
    end
  end
end
