module Transactions
  class DestroyService < Transactions::BaseService
    def initialize(transaction)
      @transaction = transaction
    end

    def execute
      @transaction.tap do |transaction|
        transaction.destroy!
      end

    rescue StandardError => e
      fail(error: e.message, resource: @transaction)
    end
  end
end
