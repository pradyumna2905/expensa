module Transactions
  class CreateService < Transactions::BaseService
    def execute
      @transaction = @current_user.transactions.build(@params)

      @transaction.tap do |transaction|
        transaction.save!
      end

    rescue ActiveRecord::RecordInvalid => e
      fail(error: e.message, resource: @transaction)
    rescue StandardError => e
      fail(error: e.message, resource: @transaction)
    end
  end
end
