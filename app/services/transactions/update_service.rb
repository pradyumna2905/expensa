module Transactions
  class UpdateService < Transactions::BaseService
    def initialize(transaction, params = {})
      @transaction = transaction
      @params = params.dup
    end

    def execute
      @transaction.tap do |transaction|
        transaction.update!(@params)
      end

    rescue ActiveRecord::RecordInvalid => e
      fail(error: e.message, resource: @transaction)
    rescue StandardError => e
      fail(error: e.message, resource: @transaction)
    end
  end
end
