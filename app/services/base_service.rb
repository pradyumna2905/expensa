class BaseService
  # TODO: Initialze service class here.
  def initialize(user, params = {})
    @current_user, @params = user, params.dup
  end

  protected

  def fail(error:, resource:)
    raise "Please provide an error message and a resource." if error.blank? || resource.blank?

    message = "Unable to save transaction. Error: #{error}"

    Rails.logger.error(message)

    resource
  end
end
