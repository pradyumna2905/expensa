module Categories
  class CreateService < Categories::BaseService
    def execute
      @category = @current_user.categories.build(@params)

      @category.tap do |category|
        category.save!
      end

    rescue ActiveRecord::RecordInvalid => e
      fail(error: e.message, resource: @category)
    rescue StandardError => e
      fail(error: e.message, resource: @category)
    end
  end
end
