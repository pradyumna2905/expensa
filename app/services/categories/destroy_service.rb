module Categories
  class DestroyService < Categories::BaseService
    def initialize(user, category)
      @current_user = user
      @category = category
    end

    def execute
      @category.tap do |category|
        ActiveRecord::Base.transaction do
          # First roll back transactions with this category to default
          # category
          sync_transactions!

          category.destroy!
        end
      end

    rescue
      fail(error: e.message, resource: @category)
    end

    private

    def sync_transactions!
      Transaction.rollback_to_default_resource(@current_user, @category)
    end
  end
end
