module Categories
  class UpdateService < Categories::BaseService
    def initialize(category, params = {})
      @category = category
      @params = params.dup
    end

    def execute
      @category.tap do |category|
        category.update!(@params)
      end

    rescue ActiveRecord::RecordInvalid => e
      fail(error: e.message, resource: @category)
    rescue StandardError => e
      fail(error: e.message, resource: @category)
    end
  end
end
