module Imports
  class CSVParseService < BaseService
    def initialize(import, params = {})
      @import, @params = import, params

      @file = @params[:file]

      @response = HashWithIndifferentAccess.new({data: [], status: :fail})
    end

    def execute
      process_csv

      @response
    end

    private
    def process_csv
      # Headers should be
      #   * date
      #   * amount
      #   * description
      #   * type/kind
      validate_headers

      if @response[:status] == :ok
        @response[:data] = parsed_csv
      end

      # if parsed_csv.size < 1
      #   @response[:status] = :fail
      #   return
      # end
    end

    def parsed_csv
      @parsed_csv ||= SmarterCSV.process(@file.path)
    end

    def validate_headers
      headers_array = CSV.read(@file.path, encoding: "windows-1251:utf-8", return_headers: true).first
      headers_array.map!(&:downcase)
      headers_array.map!(&:strip)

      unless (headers_array & [*Transaction::ATTRIBUTES]).present? &&
          (headers_array & [*Transaction::ATTRIBUTES]).size == [*Transaction::ATTRIBUTES].size
        @response[:status] = :fail
      else
        @response[:status] = :ok
      end
    end
  end
end
