module Imports
  class CreateService < BaseService
    UnprocessableCSV = Class.new(StandardError)

    # def initialize(user, params = {})
    #   @current_user, @params = user, params
    # end

    def execute
      begin
        ActiveRecord::Base.transaction do
          @import = @current_user.imports.build(import_attributes)

          @response = CSVParseService.new(@import, @params).execute

          if @response[:status] == :fail
            raise UnprocessableCSV
          else
            create_transactions
          end

          @import.save!
        end
      rescue UnprocessableCSV => e
        Rails.logger.debug e
      end

      @import
    end

    private
    def import_attributes
      {filename: "#{Date.current}-#{Time.now.strftime("%I:%M")}-imported-transactions.csv",
       content_type: "text/csv",
       payment_method: @current_user.payment_methods.find(@params[:payment_method])}
    end

    def create_transactions
      @response[:data].each do |transaction|
        Transactions::CreateService.new(@current_user, transaction_params(transaction)).execute
      end
    end

    def transaction_params(transaction)
      {date: parse_date(transaction[:date]),
       amount: parse_amount(transaction[:amount]),
       description: transaction[:description],
       kind: parse_kind(transaction[:amount]),
       user: @current_user,
       payment_method: @current_user.payment_methods.find(@params[:payment_method]),
       category: @current_user.default_category}
    end

    def parse_date(date)
      Chronic.parse(date).utc
    end

    def parse_amount(amount)
      amount&.to_f&.abs
    end

    def parse_kind(amount)
      amount&.to_f > 0 ? "credit" : "debit"
    end
  end
end
