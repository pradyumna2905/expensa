module DateHelper
  def formatted_date(date, format='abbr')
    date.strftime(t("date.formats.#{format}"))
  end
end
