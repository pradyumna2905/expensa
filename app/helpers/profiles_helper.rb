module ProfilesHelper
  def fake_masked_password
    "#{'*' * 8}".html_safe
  end

  def category_deletion_message(category)
    t('errors.fatal.delete_category', category: category.title)
  end

  def payment_method_deletion_message(payment_method)
    t('errors.fatal.delete_payment_method', payment_method: payment_method.name)
  end
end
