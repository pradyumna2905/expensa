module GraphsHelper
  def monthly_expense_total
    @transactions.debit.sum(:amount)
  end
end
