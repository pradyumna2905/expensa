module TransactionsHelper
  def transaction_deletion_message(transaction)
    t('errors.fatal.delete_transaction',
      transaction_amount: number_to_currency(transaction.amount))
  end
end
