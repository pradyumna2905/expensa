class Category < ApplicationRecord
  attr_reader :create_another

  DEFAULT_CATEGORY = "Uncategorized".freeze

  scope :default, -> { find_by(title: DEFAULT_CATEGORY) }

  belongs_to :user
  has_many :transactions

  validates_presence_of :title

  def saved?
    id? && persisted?
  end

  def updated?
    valid?
  end

  def default_category?
    self.title == DEFAULT_CATEGORY
  end

  class << self
    def expenses_by_month(start_date: Date.current, end_date: Date.current)
      group(:title).joins(:transactions).
        where("transactions.date >= ? AND transactions.date <= ? AND transactions.kind = ?",
              start_date.beginning_of_month, end_date.end_of_month, 0)
    end
  end
end
