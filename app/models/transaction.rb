class Transaction < ApplicationRecord
  attr_reader :create_another

  SANITZED_YEARS = [*5.years.ago.year..Date.current.year].freeze
  ATTRIBUTES = %w(date amount description)

  # ============================== ASSOCIATIONS =============================
  belongs_to :user
  belongs_to :payment_method
  belongs_to :category

  # Descend by date
  scope :desc, -> { order(date: :desc) }

  # If no argument is provided to `highest_expense_transactions` scope, it will
  # return 100 records by default.
  scope :highest_expense_transactions, -> (limit = 100) { debit.order("amount DESC").limit(limit) }

  # ============================== VALIDATIONS  =============================
  validates_presence_of :date, :amount, :description
  validates :amount, numericality: { greater_than: 0 }
  validates :description, length: { minimum: 2 }

  validate :future_transaction

  accepts_nested_attributes_for :payment_method
  accepts_nested_attributes_for :category

  # Type of transaction
  enum kind: { debit: 0, credit: 1 }

  def saved?
    id && persisted?
  end

  def updated?
    valid?
  end

  class << self
    def rollback_to_default_resource(user, resource)
      raise "Provide a user and resource" if user.blank? || resource.blank?

      transactions = user.transactions.where(
        "#{resource.class.name.underscore}_id": resource.id
      )

      if transactions.any?
        transactions.update(
          "#{resource.class.name.underscore}": user.default_resource(resource)
        )
      end
    end

    # TODO: Refactor
    #
    # Combine `by_month` and `group_by_date_range`
    def group_by_date_range(start_date: Date.current, end_date: Date.current, format: :monthly)
      if format == :monthly
        group_by_day_of_month(:date,
                              range: start_date.beginning_of_month..end_date.end_of_month)
      elsif format == :weekly
        where(date: start_date..end_date)
      else
        raise NotImplementedError
      end
    end

    def by_date_range(start_date: Date.current.beginning_of_month, end_date: Date.current.end_of_month)
      where(date: start_date..end_date)
    end

    def filter_by_params(index_params)
      _transactions = by_month(index_params&.dig(:month),
                      index_params&.dig(:year))

      if index_params&.dig(:category)
        _transactions = _transactions.joins(:category).where(
          "categories.title = ?", index_params.dig(:category)
        )
      end

      _transactions
    end

    # You can pass month as a String like "November" or as an integer like 11
    # or as a string-integer like "11".
    #
    # Do not pass datetime objects to this method.
    def by_month(month, year)
      where(
        date: start_date(month, year).beginning_of_month..end_date(month, year).end_of_month
      ).desc
    end

    def current_month
      by_month(Date.current.month, Date.current.year)
    end

    def total_expense
      debit.sum(:amount)
    end

    def total_income
      credit.sum(:amount)
    end

    private
    def start_date(month, year)
      return Date.current if month.blank? || year.blank?

      _month_index = month_index(month)
      _year = sanitize_year(year)

      Date.new(_year, _month_index, 1)
    end

    def end_date(month, year)
      start_date(month, year).end_of_month
    end

    def month_index(month)
      if month.to_i.zero?
        Date::MONTHNAMES.index(month).presence || Date.current.month
      else
        month&.to_i < 12 ? month&.to_i : Date.current.month
      end
    end

    def sanitize_year(year)
      SANITZED_YEARS.include?(year&.to_i) ? year.to_i : Date.current.year
    end
  end

  def future_transaction
    # Dont allow future transactions for now
    if self.date.present? && self.date > Date.current
      errors.add(:date, "can't be in the future.")
    end
  end
end
