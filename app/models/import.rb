class Import < ApplicationRecord
  has_many :transactions
  has_one :payment_method

  belongs_to :user

  validates_uniqueness_of :filename

  accepts_nested_attributes_for :transactions
end
