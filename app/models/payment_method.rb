class PaymentMethod < ApplicationRecord
  DEFAULT_PAYMENT_METHOD = "Cash".freeze

  scope :default, -> { find_by(name: DEFAULT_PAYMENT_METHOD) }

  belongs_to :user
  has_many :transactions

  validates_presence_of :name

  def updated?
    valid?
  end

  def default_payment_method?
    self.name == DEFAULT_PAYMENT_METHOD
  end

  class << self
    def expenses_by_month(start_date: Date.current, end_date: Date.current)
      group(:name).joins(:transactions).
        where("transactions.date >= ? AND transactions.date <= ? AND transactions.kind = ?",
              start_date.beginning_of_month, end_date.end_of_month, 0)
    end
  end
end
