class PaymentMethodsController < ApplicationController
  before_action :find_payment_method, except: [:new, :create, :index]

  def new
    @payment_method = current_user.payment_methods.build
  end

  def create
    @payment_method = current_user.payment_methods.build(payment_method_params)

    if @payment_method.save
      redirect_to new_transaction_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    @payment_method = PaymentMethods::UpdateService.new(@payment_method,
                                                        payment_method_params).execute

    if @payment_method.updated?
      redirect_to profile_user_path
    else
      render :edit
    end
  end

  def destroy
    @payment_method = PaymentMethods::DestroyService.new(current_user,
                                                         @payment_method).execute

    message = 'Your payment method was deleted successfully.' if @payment_method.destroyed?

    flash[:danger] = message
    redirect_to profile_user_path(current_user)
  end

  private

  def find_payment_method
    @payment_method = current_user.payment_methods.find(params[:payment_method_id])
  end

  def payment_method_params
    params.require(:payment_method).permit(:name)
  end
end
