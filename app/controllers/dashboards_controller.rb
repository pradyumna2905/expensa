class DashboardsController < ApplicationController
  include DatetimeSanitizer

  before_action :set_month_year_label

  def current
    @transactions = current_user.transactions.by_month(
      current_params&.dig(:month),
      current_params&.dig(:year)
    )

    @weekly_transactions = transactions_grouped_by_week
    @highest_expense_transactions = current_user.transactions.by_date_range(
      start_date: 7.days.ago, end_date: Date.current
    ).highest_expense_transactions(10)

    @weekly_income = current_user.transactions.credit.by_date_range(
      start_date: 7.days.ago, end_date: Date.current
    ).desc
    @category_expenses_by_selected_month = category_expenses_by_month
    @payment_method_expenses_by_selected_month = payment_method_expenses_by_month

    @monthly_expenses = @transactions.total_expense
    @monthly_income = @transactions.total_income
  end

  def trends
    @transactions = current_user.transactions.by_month(
      current_params&.dig(:month),
      current_params&.dig(:year)
    )

    @transactions_grouped_by_month = transactions_grouped_by_month
    @category_expenses_by_selected_month = category_expenses_by_month
    @payment_method_expenses_by_selected_month = payment_method_expenses_by_month

    @monthly_expenses = @transactions.total_expense
    @monthly_income = @transactions.total_income
  end

  private

  def current_params
    params.require(:date).permit(:year, :month) if params[:date]
  end

  def category_expenses_by_month
    current_user.categories.expenses_by_month(
      start_date: derive_datetime, end_date: derive_datetime
    )
  end

  def payment_method_expenses_by_month
    current_user.payment_methods.expenses_by_month(
      start_date: derive_datetime, end_date: derive_datetime
    )
  end

  def transactions_grouped_by_week
    current_user.transactions.
      group_by_date_range(start_date: 7.days.ago, end_date: Date.current, format: :weekly)
  end

  def transactions_grouped_by_month
    current_user.transactions.
      group_by_date_range(start_date: derive_datetime, end_date: derive_datetime)
  end

  def month_name_label
    if current_params.present?
      derive_month(current_params.dig(:month))
    else
      current_month
    end
  end

  def year_label
    if current_params.present?
      derive_year(current_params.dig(:year))
    else
      current_year
    end
  end
end
