class CategoriesController < ApplicationController
  before_action :find_category, except: [:new, :create, :index]

  def new
    @category = current_user.categories.build
  end

  def create
    @category = Categories::CreateService.new(current_user,
                                              category_params).execute

    if @category.saved?
      if params.dig(:category, :create_another) == "1"
        redirect_to new_category_path
      else
        redirect_to new_transaction_path
      end
    else
      render :new
    end
  end

  def edit
  end

  def update
    @category = Categories::UpdateService.new(@category,
                                              category_params).execute

    if @category.updated?
      redirect_to profile_user_path
    else
      render :edit
    end
  end

  def destroy
    @category = Categories::DestroyService.new(current_user,
                                               @category).execute

    message = 'Your category was deleted successfully.' if @category.destroyed?

    flash[:danger] = message
    redirect_to profile_user_path(current_user)
  end

  private

  def find_category
    @category = current_user.categories.find(params[:category_id])
  end

  def category_params
    params.require(:category).permit(:title)
  end
end
