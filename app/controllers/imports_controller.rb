class ImportsController < ApplicationController
  def new
    @import = Import.new
  end

  def create
    @import = Imports::CreateService.new(current_user,
                                         import_params).execute

    if @import.persisted?
      flash[:success] = "Transactions imported!"
      redirect_to transactions_path
    else
      flash[:danger] = "Something went wrong! Please try again!"
      render :new
    end
  end

  private

  def import_params
    params.permit(import_params_attributes)
  end

  def import_params_attributes
    [
      :file,
      :payment_method
    ]
  end
end
