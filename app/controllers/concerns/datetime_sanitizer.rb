module DatetimeSanitizer
  extend ActiveSupport::Concern

  def derive_datetime
    Date.new(@selected_year, Date::MONTHNAMES.index(@selected_month), 1)
  end

  def beginning_of_week
  end

  def set_month_year_label
    @selected_month ||= month_name_label
    @selected_year ||= year_label
  end

  def derive_month(month)
    Date::MONTHNAMES[month&.to_i].presence || current_month
  end

  def derive_year(year)
    if Transaction::SANITZED_YEARS.include?(year.to_i)
      year.to_i
    else
      current_year
    end
  end

  def current_month
    Date::MONTHNAMES[Date.current.month]
  end

  def current_year
    Date.current.year
  end
end
