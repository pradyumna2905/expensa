class TransactionsController < ApplicationController
  include DatetimeSanitizer

  before_action :authenticate_user!
  before_action :set_transactions, except: [:new]

  before_action :set_month_year_label, only: [:index]

  before_action :find_transaction, except: [:new, :create, :index]

  def new
    @transaction = Transaction.new
  end

  def create
    @transaction = Transactions::CreateService.new(current_user,
                                                   transaction_params).execute

    if @transaction.saved?
      if params.dig(:transaction, :create_another) == "1"
        redirect_to new_transaction_path
      else
        redirect_to transactions_path
      end
    else
      render :new
    end
  end

  def index
    _transactions = current_user.transactions.filter_by_params(index_params).
      includes(:payment_method, :category).desc

    @transactions = Kaminari.paginate_array(_transactions).page(params[:page])
    @monthly_expenses = _transactions.total_expense
    @monthly_income = _transactions.total_income
  end

  def edit
  end

  def update
    @transaction = Transactions::UpdateService.new(@transaction,
                                                   transaction_params).execute

    if @transaction.updated?
      redirect_to transactions_path
    else
      render :edit
    end
  end

  def destroy
    @transaction = Transactions::DestroyService.new(@transaction).execute

    message = "Transaction deleted successfully" if @transaction.destroyed?

    flash[:danger] = message
    redirect_to transactions_path
  end

  private

  def find_transaction
    @transaction = current_user.transactions.find(params[:id])
  end

  def index_params
    if params[:date].present? && params[:category].present?
      _params = params.require(:date).permit(:year, :month)
      _params.merge(params.permit(:category))
    elsif params[:date].present?
      params.require(:date).permit(:year, :month)
    elsif params[:category].present?
      params.permit(:category)
    end
  end

  def set_transactions
    @transactions = current_user.transactions
  end

  def transaction_params
    params.require(:transaction).permit(:date,
                             :amount,
                             :description,
                             :payment_method_id,
                             :category_id,
                             :kind,
                             :user_id)
  end

  def month_name_label
    if index_params&.dig(:month).present?
      derive_month(index_params.dig(:month))
    else
      current_month
    end
  end

  def year_label
    if index_params&.dig(:year).present?
      derive_year(index_params.dig(:year))
    else
      current_year
    end
  end
end
