source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.0.2'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'bootstrap-sass', '~> 3.3.6'
gem 'devise'
gem 'simple_form'
gem 'inline_svg'
gem 'kaminari', github: "amatsuda/kaminari"
gem 'chartkick'
gem 'groupdate'
gem 'gemoji'
gem 'font-awesome-rails'
gem 'smarter_csv'
gem 'awesome_print'
gem 'chronic'
gem 'active_link_to', '~> 1.0'

# Application Settings
gem 'settingslogic', '~> 2.0.9'

# Sentry integration (error reporting)
gem 'sentry-raven', '~> 2.7.1'

# Environment Variables Handler
gem 'figaro'

# HAML
gem 'hamlit'

gem 'passenger', '5.1.12'

# Colored terminal output
gem 'rainbow', '3.0'

# Seed data
gem 'seed-fu', '~> 2.3.5'

# Fake data
gem 'faker', github: 'stympy/faker'

group :production do
  gem 'pg'
end

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'rspec-rails', '~> 3.5'
  gem 'shoulda-matchers'
  gem 'factory_girl_rails'
  gem 'simplecov', require: false
  gem 'pry'
  gem 'pry-nav'
  gem 'rails-controller-testing'
  gem 'capybara'
  gem 'capybara-webkit'
  gem 'selenium-webdriver', '~> 3.8.0'
  gem 'chromedriver-helper'

  # Deployment
  gem 'capistrano', '~> 3.10'
  # gem 'capistrano3-puma'
  gem 'capistrano-rails', '~> 1.3'
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rvm'
  gem 'capistrano-passenger', '~> 0.2.0'
end

group :test do
  gem 'database_cleaner'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'bullet'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
