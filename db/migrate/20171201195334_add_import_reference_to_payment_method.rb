class AddImportReferenceToPaymentMethod < ActiveRecord::Migration[5.0]
  def change
    add_reference :payment_methods, :import, index: true, null: true
  end
end
