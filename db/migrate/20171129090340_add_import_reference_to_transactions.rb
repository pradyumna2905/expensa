class AddImportReferenceToTransactions < ActiveRecord::Migration[5.0]
  def change
    add_reference :transactions, :import, index: true
  end
end
