class AddUserReferenceToImports < ActiveRecord::Migration[5.0]
  def change
    add_reference :imports, :user, index: true, foreign_key: true
  end
end
