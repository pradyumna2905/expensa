class CreateImports < ActiveRecord::Migration[5.0]
  def change
    create_table :imports do |t|
      t.string :filename
      t.string :content_type

      t.timestamps
    end
  end
end
