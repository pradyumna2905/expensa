require 'rainbow/ext/string'

Expensa::Seeder.quiet!
Expensa::Seeder.stdout('=== Seeding Payment Methods ===', :cyan)

PaymentMethod.seed do |s|
  names = [
    'Chase',
    'Bank of America',
    'Wells Fargo',
    'Citibank',
    'TD Bank',
    'Amex',
    'Capital One',
    'Discover',
    'US Bank',
    'PNC',
    'USAA'
  ]
  # Default payment methods on a user
  PM_DEFAULT_COUNT = 6

  count = ENV.fetch('PM_COUNT') { PM_DEFAULT_COUNT }

  # Keeping it here so when we have additional attributes to payment_methods,
  # we just add there here.
  payment_method_params = {
    user: User.first
  }

  names.first(count).each do |name|
    # Add the payment method name
    payment_method_params.merge!({name: name})

    payment_method = PaymentMethod.create(payment_method_params)

    Expensa::Seeder.print_result(payment_method)
  end
  puts "\n\n"
end
