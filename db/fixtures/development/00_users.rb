require 'rainbow/ext/string'

Expensa::Seeder.quiet!
Expensa::Seeder.stdout('=== Seeding User ===', :cyan)

User.seed do |s|
  s.id       = 1
  s.email    = 'john@expensa.com'
  s.password = 'expen$a'
end
