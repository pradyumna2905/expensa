# This is a poor man's way of doing everything in this class. Will improve
# later.
require 'rainbow/ext/string'

Expensa::Seeder.quiet!
Expensa::Seeder.stdout('=== Seeding Transactions ===', :cyan)
Expensa::Seeder.stdout('This might take a while', :cyan)

MONTHLY_TRANSACTION_COUNT = ENV.fetch('TRANSACTIONS_COUNT') { 50 }

Transaction.seed do |s|
  current_date = Date.current

  expense_params = {
    kind: 'debit',
    user: User.first,
    payment_method: PaymentMethod.all.sample,
    category: Category.all.sample
  }

  Expensa::Seeder.stdout('=== Seeding Expenses ===', :indianred)
  [*1..12].each do |month_number|
    # Display counter. We don't want to %^&* with the dev.
    c = 0

    MONTHLY_TRANSACTION_COUNT.times do
      # Increment counter
      c += 1

      expense_params.merge!({
        date: Faker::Date.between(month_number.months.ago, (month_number-1).months.ago),
        amount: Faker::Number.between(1, 100),
        description: Faker::Lorem.sentence
      })
      transaction = Transaction.create(expense_params)

      if transaction.valid? && transaction.persisted?
        # Chill. Display `.` every 100 times.
        if (c % 10) == 0
          print '.'.color(:green)
        end
      else
        print 'F'.color(:red)
      end
    end
  end
end
puts "\n\n"

Transaction.seed do |s|
  income_params = {
    kind: 'credit',
    description: 'Paycheck',
    user: User.first,
    payment_method: PaymentMethod.all.sample,
    category: Category.first
  }

  Expensa::Seeder.stdout('=== Seeding Income ===', :magenta)
  [*1..12].each do |month_number|
    2.times do
      income_params.merge!({
        date: Faker::Date.between(month_number.months.ago, (month_number-1).months.ago),
        amount: Faker::Number.between(2000, 10000),
      })
      transaction = Transaction.create(income_params)

      if transaction.valid? && transaction.persisted?
        print '.'.color(:green)
      else
        print 'F'.color(:red)
      end
    end
  end
end
