require 'rainbow/ext/string'

Expensa::Seeder.quiet!
Expensa::Seeder.stdout('=== Seeding Categories ===', :cyan)

Category.seed do |s|
  titles = [
    'Housing',
    'Utilities',
    'Food & Drink',
    'Transportation',
    'Education',
    'Entertainment',
    'Gifts & Donations',
    'Personal Care',
    'Pets',
    'Health & Fitness',
    'Services',
    'Fees & Charges',
    'Kids',
    'Miscellaneous Expense',
    'Transfer',
    'Taxes'
  ]
  # For now, consider all of them
  CATEGORY_DEFAULT_COUNT = titles.count

  count = ENV.fetch('CATEGORIES_COUNT') { CATEGORY_DEFAULT_COUNT }

  category_params = {
    user: User.first
  }

  titles.first(count).each do |title|
    # Add the category title
    category_params.merge!({title: title})

    category = Category.create(category_params)

    Expensa::Seeder.print_result(category)
  end
  puts "\n\n"
end
