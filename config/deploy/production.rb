# set :server_ip, "52.34.148.146"

server fetch(:server_ip), user: "deploy", roles: %w{app db web}, primary: true

set :ssh_options, {
                    keys: %w(/home/pradyumna/.ssh/et_rsa),
                    forward_agent: false,
                    auth_methods: %w(publickey)
                  }

