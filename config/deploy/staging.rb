set :server_ip, "52.42.138.175"
set :stage, :staging

server fetch(:server_ip), user: "deploy", roles: %w{app db web}, primary: true

set :ssh_options, {
                    keys: %w(/home/pradyumna/.ssh/ex_rsa),
                    forward_agent: false,
                    auth_methods: %w(publickey)
                  }


