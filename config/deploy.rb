set :rvm_ruby_version, "2.4.1@ex"

set :user, "deploy"
set :application, "expensa"
set :repo_url, "git@gitlab.com:pradyumna2905/expensa.git"

# Default branch is :master
set :branch, ask("the tag/branch to deploy. Latest tag: #{`git describe --abbrev=0 --all`.chomp}. Default:", :master)

set :deploy_to, "/home/#{fetch(:user)}/#{fetch(:application)}"

set :bundle_flags, "--deployment"

# Default value for :pty is false
set :pty, true
set :use_sudo, false

# Link config files
set :linked_files, %w{config/database.yml config/secrets.yml config/application.yml}

# Link dirs
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # invoke 'puma:restart'
    end
  end

  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end

