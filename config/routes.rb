Rails.application.routes.draw do
  devise_for :users

  devise_scope :user do
    unauthenticated :user do
      root to: 'devise/registrations#new'
    end

    authenticated :user do
      root to: 'dashboards#current', as: :authenticated_root
    end
  end

  resources :users, only: :none do
    member do
      resources :payment_methods, except: [:index, :show],
        path: 'wallet', param: :payment_method_id
      resources :categories, except: [:index, :show], param: :category_id

      get '/profile', to: 'profiles#show'
    end
  end

  namespace :dashboards do
    get :current
    get :trends
  end

  resources :transactions do
    collection do
      post '/filter', to: 'transactions#filter'
      resources :imports, only: [:new, :create]
    end
  end
end
